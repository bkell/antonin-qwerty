\documentclass{article}

\usepackage{float}
\usepackage[pdftex]{graphicx}
\usepackage{pdfpages}
\usepackage{rotating}

\renewcommand{\abstractname}{Concrete}

\title{Toward a historically faithful performance of the piano works of
       Anton\'in Qwe\v rt\'y}
\author{William Gunther\\
        \small{Google, Inc.}\\
        \small{wgunther@google.com}
        \and
        Brian Kell\\
        \small{Google, Inc.}\\
        \small{bkell@google.com}}

\date{\small{\strut SIGBOVIK '18}\\
      \small{\strut Carnegie Mellon University}\\
      \small{\strut April~$-2$, 2018}}

\begin{document}
\maketitle

\begin{abstract}
The great Czech composer Anton\'in Dvo\v r\'ak (1841--1904) wrote many pieces
for the piano, including the famous \textit{Humoresque No.~7 in G-flat
Major}~\cite{humoresque}. Unfortunately, typical performances of these works
today sound nothing like what the composer intended because most modern pianos
are configured with a different keyboard layout. Through painstaking historical
research, we have reconstructed the original Dvo\v r\'ak piano keyboard layout.
We have applied this discovery by transposing the \textit{Humoresque} so that it
is playable on a modern piano, enabling the first historically faithful
performance of this piece in over a century.
\end{abstract}

\clearpage

\begin{figure}[H]
  \centering
  \includegraphics[width=4.5in]{KB_DSKtypewriter}
  \caption{
    A Dvorak keyboard with the original or ``classic''
    layout~\cite{dvorak-image}. There are several variants of the Dvorak layout,
    but Dvo\v r\'ak was a classical composer, so this is almost certainly the
    one he used. Furthermore, this layout has 44~white keys (not counting the
    spacebar, which is clearly used only for rests). That is exactly half of the
    number of keys on a piano. Thus we may confidently conclude that the left
    half of Dvo\v r\'ak's piano layout was just these 44~keys, while the right
    half was the same keys again with the Shift key held down.
  }
\end{figure}

\newbox\whoneedsem
\setbox\whoneedsem=\hbox{\texttt{\char`\`\char`\~\char`\]\char`\}\char`\\\char`\|}}

\begin{figure}[H]
  \centering
  \includegraphics[width=4.5in]{KB_United_States}
  \caption{
    A modern QWERTY keyboard with the United States layout~\cite{qwerty-image}.
    This layout has 47~white keys (not counting the spacebar), but obviously
    three of them are useless: nobody really needs the characters
    \copy\whoneedsem~\cite{bringhurst}. This leaves 44~keys in the same
    positions as the keys of the Dvorak keyboard, which can then be mapped to
    the piano in the same way.
  }
\end{figure}

\begin{sidewaysfigure}
  \centering
  \includegraphics[width=7.5in]{keyboard-dvorak}
  \caption{
    The reconstructed Dvo\v r\'ak keyboard layout. Although it looks strange to
    modern eyes, this keyboard would have looked familiar to Anton\'in
    Dvo\v r\'ak in~1894 and is unquestionably the layout for which he composed
    the \textit{Humoresques} and his other piano works.
  }
  \vskip 1in
  \includegraphics[width=7.5in]{keyboard-qwerty}
  \caption{
    The familiar Qwe\v rt\'y keyboard layout, used by most modern pianos. The
    mapping from keys of the Dvo\v r\'ak keyboard to those of the Qwe\v rt\'y
    keyboard is in most cases obvious. The main difficulty lies in the keys
    ${}^1\!/\!_4$,~\hbox{\rlap/c}, and~${}^1\!/\!_2$ on the Dvo\v r\'ak
    keyboard, which do not appear on the Qwe\v rt\'y keyboard. While a modern
    Qwe\v rt\'y piano cannot reproduce the note~${}^1\!/\!_4$ exactly, we can
    come close by playing 1,~/, and~4 simultaneously. Likewise, we can
    approximate \hbox{\rlap/c} and~${}^1\!/\!_2$ with the chords
    $\{\hbox{c},\hbox{/}\}$ and $\{1,\hbox{/},2\}$, respectively.
  }
\end{sidewaysfigure}

\clearpage

\includepdf[pages=-]{sheet_music/humoresque_no_7.pdf}
\includepdf[pages=-]{sheet_music/humoresque_no_7_qwertified_manual_tweaks.pdf}

\begin{thebibliography}{9}

\bibitem{bringhurst} Bringhurst, Robert. \textit{The Elements of Typographic
Style}, version~3.1. Hartley \& Marks, 2005. For example, Bringhurst dismisses
the tilde key: ``In the eyes of ISO and Unicode, the swung dash found on
computer keyboards is an \textit{ascii tilde}---a character \ldots\ meaningless
to typographers.'' He describes the backslash as ``an unsolicited gift of the
computer keyboard'' with ``no accepted function in typography.'' And of the pipe
character, he writes, ``Despite \ldots\ its presence on the standard ASCII
keyboard, the pipe has no function in typography. This is another key, and
another slot in the font, that begs to be reassigned to something
typographically useful.''

\bibitem{humoresque} Dvo\v r\'ak, Anton\'in. \textit{Humoresque No.~7 in G-flat
Major}, Op.~101, S.~123. N.~Simrock, London, 1894. Reprinted in
\textit{Humoresques \& Other Works for Solo Piano}, Dover Publications, 1994.

\bibitem{dvorak-image} Optikos at English Wikipedia. \textit{File:KB
DSKtypewriter.svg}. February~10, 2010. https://commons.wikimedia.org\slash
wiki\slash File:KB\_DSKtypewriter.svg. Licensed under the Creative Commons
Attribution-Share Alike 3.0 Unported license and the GNU Free Documentation
License.

\bibitem{qwerty-image} Wikimedia Commons contributors (Denelson83, Bodigami,
Bencherlite, and Yes0song). \textit{File:KB United States.svg}. December~27,
2010. https://commons.wikimedia.org\slash wiki\slash
File:KB\_United\_States.svg. Licensed under the GNU Free Documentation License
and the Creative Commons Attribution-Share Alike 3.0 Unported license.

\end{thebibliography}

\noindent Thanks to William Lovas for his piano performance during our
presentation at the SIGBOVIK conference.

\end{document}
