import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Receiver;
import javax.sound.midi.Track;
import javax.sound.midi.Transmitter;

class MidiNoteTranslator {
  private static final int kPianoOffset = 21;
  private static final int kOneQuarter = -1;
  private static final int kCentSign = -2;
  private static final int kOneHalf = -3;

  public MidiNoteTranslator(HashMap<Integer, Integer> map,
                            HashMap<String, Integer> translate_map) {
    dvorak_to_qwerty_ = map;
    dvorak_letter_to_key_ = translate_map;
    translate_ = true;
  }

  public ArrayList<Integer> Translate(int midi_note) {
    ArrayList<Integer> notes = new ArrayList<Integer>();
    if (translate_) {
      int note = dvorak_to_qwerty_.get(midi_note);
      if (note > 0) {
        notes.add(note);
      }
      else {
        switch (note) {
          case kOneQuarter:
            notes.add(dvorak_letter_to_key_.get("1"));
            notes.add(dvorak_letter_to_key_.get("/"));
            notes.add(dvorak_letter_to_key_.get("4"));
            break;
          case kOneHalf:
            notes.add(dvorak_letter_to_key_.get("1"));
            notes.add(dvorak_letter_to_key_.get("/"));
            notes.add(dvorak_letter_to_key_.get("2"));
            break;
          case kCentSign:
            notes.add(dvorak_letter_to_key_.get("/"));
            notes.add(dvorak_letter_to_key_.get("c"));
            break;
        }
      }
    } else {
      notes.add(midi_note);
    }
    return notes;
  }

  public void Toggle() {
    translate_ = !translate_;
  }

  static MidiNoteTranslator FromConfig(String config_file_path)
      throws Exception {
    BufferedReader bufferedReader =
        new BufferedReader(new FileReader(config_file_path));
    String line;
    HashMap<Integer, Integer> out_map = new HashMap<Integer, Integer>();
    HashMap<String, Integer> translate_map = new HashMap<String, Integer>();
    ArrayList<String> key_to_dvorak = new ArrayList<String>();
    HashMap<String, Integer> qwerty_to_key = new HashMap<String, Integer>();
    while((line = bufferedReader.readLine()) != null) {
      if (line.length() > 0) {
        int key_number = key_to_dvorak.size();
        String[] split_line = line.split("\t");
        String dvorak = split_line[0];
        String qwerty = split_line[1];
        qwerty_to_key.put(qwerty, key_number);
        key_to_dvorak.add(key_number, dvorak);
      }
    }
    for (int key = 0; key < key_to_dvorak.size(); ++key) {
      int input = key + kPianoOffset;
      String dvorak_letter = key_to_dvorak.get(key);
      int output;
      if (qwerty_to_key.containsKey(dvorak_letter)) {
        output = qwerty_to_key.get(key_to_dvorak.get(key)) + kPianoOffset;
        translate_map.put(dvorak_letter, output);
      } else {
        switch (dvorak_letter) {
          case "char(14)":  // 1/4
            output = kOneQuarter;
            break;
          case "char(1)":  // cent sign
            output = kCentSign;
            break;
          case "char(12)":  // 1/2
            output = kOneHalf;
            break;
          default:
            throw new Exception("Uh-oh");
        }
      }
      out_map.put(input, output);
    }
    return new MidiNoteTranslator(out_map, translate_map);
  }


  private HashMap<String, Integer> dvorak_letter_to_key_;
  private HashMap<Integer, Integer> dvorak_to_qwerty_;
  private boolean translate_;
}

class MidiTranslator {
  public MidiTranslator(MidiNoteTranslator note_translator) {
    note_translator_ = note_translator;
  }

  public ArrayList<MidiMessage>
      TranslateMidiMessage( MidiMessage message) throws Exception {
    ArrayList<MidiMessage> list = new ArrayList<MidiMessage>();
    if (message instanceof ShortMessage) {
      ShortMessage short_message = (ShortMessage)message;
      switch (short_message.getCommand()) {
        case ShortMessage.NOTE_ON:
        case ShortMessage.NOTE_OFF:
          if (verbose_) {
            System.out.println("Received short message note:");
            System.out.println("\tchannel:" + short_message.getChannel());
            System.out.println("\tdata1: " + short_message.getData1());
            System.out.println("\tdata2: " + short_message.getData2());
          }
          int velocity = short_message.getData2();
          ArrayList<Integer> keys =
              note_translator_.Translate(short_message.getData1());
          for (int key : keys) {
            ShortMessage message_to_add = (ShortMessage)short_message.clone();
            message_to_add.setMessage(short_message.getCommand(),
                short_message.getChannel(), key, velocity);
            list.add(message_to_add);
          }
          break;
        default:
          if (verbose_) {
            System.out.println("Received short message non-note.");
          }
          list.add(short_message);
          // modify nothing.
      }
    } else {
      if (verbose_) {
        System.out.println("Received non-short message.");
      }
        list.add(message);
    }
    return list;
  }

  public ArrayList<MidiEvent>
      TranslateMidiTrack(Track track) throws Exception {
    ArrayList<MidiEvent> events = new ArrayList<MidiEvent>();
    for (int i = 0; i < track.size(); ++i) {
      MidiEvent event_to_translate = track.get(i);
      ArrayList<MidiMessage> messages =
          TranslateMidiMessage(event_to_translate.getMessage());
      for (MidiMessage message : messages) {
        MidiEvent event =
            new MidiEvent(message, event_to_translate.getTick());
        events.add(event);
      }
    }
    return events;
  }

  public void TranslateMidiSequence(Sequence sequence) throws Exception {
    ArrayList<Track> tracks_to_delete = new ArrayList<Track>();
    ArrayList<ArrayList<MidiEvent>> tracks_to_add =
        new ArrayList<ArrayList<MidiEvent>>();
    for (Track track : sequence.getTracks()) {
      ArrayList<MidiEvent> events = TranslateMidiTrack(track);
      tracks_to_add.add(events);
      tracks_to_delete.add(track);
    }
    for (Track track : tracks_to_delete) {
      sequence.deleteTrack(track);
    }
    for (ArrayList<MidiEvent> track_to_add : tracks_to_add) {
      Track new_track = sequence.createTrack();
      for (MidiEvent event : track_to_add) {
        new_track.add(event);
      }
    }
  }

  public void Toggle() {
    note_translator_.Toggle();
  }

  public boolean verbose_;
  private MidiNoteTranslator note_translator_;
}

class MidiTranslatorReciever implements Receiver {
  MidiTranslatorReciever(Receiver receiver, MidiTranslator translator) {
    receiver_ = receiver;
    translator_ = translator;
  }

  public void close() {
    receiver_.close();
  }

  public void send(MidiMessage message, long timeStamp) {
    try {
      ArrayList<MidiMessage> messages =
          translator_.TranslateMidiMessage(message);
      for (MidiMessage translated_message : messages) {
        receiver_.send(translated_message, 0);
      }
    } catch (Exception e) {
      System.out.println("Caught error: " + e);
    }
  }

  Receiver receiver_;
  MidiTranslator translator_;
}

class CommandListener {
  CommandListener(Receiver receiver, MidiTranslator translator) {
    receiver_ = receiver;
    translator_ = translator;
  }

  public void Listen() {
    Scanner input = new Scanner(System.in);
    while (input.hasNext()) {
      ExecuteCommand(input.nextLine());
    }
  }

  private void ExecuteCommand(String command_line) {
    String[] pieces = command_line.split(" ");
    String command = pieces[0];
    if (command.equals("toggle")) {
      translator_.Toggle();
    } else if (command.equals("exit")) {
      receiver_.close();
      System.exit(0);
    } else if (command.equals("verbose")) {
      if (pieces[1].equals("on")) {
        translator_.verbose_ = true;
      }
      if (pieces[1].equals("off")) {
        translator_.verbose_ = false;
      }
    }
    else {
      System.out.println("Invalid command: " + command);
    }
  }

  Receiver receiver_;
  MidiTranslator translator_;
}

public class Main {

  public static void main(String[] args) throws Exception {
    if (args.length != 2 && args.length != 1) {
      System.out.println("Need to pass in config file then optional midi");
      System.exit(1);
    }
    MidiNoteTranslator note_translator =
        MidiNoteTranslator.FromConfig(args[0]);
    MidiTranslator translator = new MidiTranslator(note_translator);
    if (args.length == 2) {
      File mid_file = new File(args[1]);
      Sequence sequence = MidiSystem.getSequence(mid_file);
      translator.TranslateMidiSequence(sequence);
      MidiSystem.write(sequence, 1, System.out);
    } else {
      MidiDevice.Info[] midi_device_infos = MidiSystem.getMidiDeviceInfo();
      for (MidiDevice.Info midi_device_info : midi_device_infos) {
        System.out.println(midi_device_info);
      }
      Transmitter base_transmitter = MidiSystem.getTransmitter();
      Synthesizer synth = MidiSystem.getSynthesizer();
      Receiver base_receiver = synth.getReceiver();
      synth.open();


      System.out.println("receiver: " + base_receiver);
      System.out.println("transmitter: " + base_transmitter);
      System.out.println("latency: " + synth.getLatency());

      Receiver translation_receiver =
          new MidiTranslatorReciever(base_receiver, translator);
      base_transmitter.setReceiver(translation_receiver);
      CommandListener listener =
          new CommandListener(translation_receiver, translator);
      listener.Listen();
    }
  }
}
