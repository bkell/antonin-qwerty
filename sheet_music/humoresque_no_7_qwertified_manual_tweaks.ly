\version "2.18.2"
\language "english"

\header {
  title = "Jfmsod;xfd No. 2 in A-flat Harmonic Major ♭6 ♭5"
  composer = "Antonín Qweřtý"
  tagline = ""
}

harmonicMajorFlatSixFive =
    #`((0 . ,NATURAL) (1 . ,NATURAL) (2 . ,NATURAL) (3 . ,NATURAL) (4 . ,FLAT)
       (5 . ,DOUBLE-FLAT) (6 . ,NATURAL))
harmonicMajorFlatFive =
    #`((0 . ,NATURAL) (1 . ,NATURAL) (2 . ,NATURAL) (3 . ,NATURAL) (4 . ,FLAT)
       (5 . ,FLAT) (6 . ,NATURAL))

leggiero = \tweak DynamicText.self-alignment-X #LEFT
           #(make-dynamic-script (markup #:normal-text #:italic "leggiero"))
sustainOffSenza = \markup { \musicglyph #"pedal.*" \italic senza }
sustainOnOff = \markup {
                 \concat { \musicglyph #"pedal.Ped" \musicglyph #"pedal.." }
                 \musicglyph #"pedal.*"
               }
dimin = \markup { \italic "dimin." }
ritard = \markup { \italic "ritard." }
inTempo = \markup { \italic "in tempo" }
cresc = \markup { \italic "cresc." }
dim = \markup { \italic "dim." }
pDim = \markup { \dynamic p \italic "dim." }

\new GrandStaff <<
  \new Staff \with { \consists "Span_arpeggio_engraver" } {
    \set Staff.connectArpeggios = ##t
    \tempo "Poco Lento e grazioso."
    \clef treble
    \key af \harmonicMajorFlatSixFive
    \time 2/4
    g'16\([_\leggiero r32 eff''32 g'16 r32 eff''32]
        bff'16[ r32 af'32 c''16\< r32 af'32]\) |
    \acciaccatura c''8 eff'16\([ r32 df''''32 eff'''16\! r32 eff'32]
        df''''16[ r32 eff'''32 eff'16 r32 c''32]\) |
    \acciaccatura bff'8 af'16\([\> r32 af'32 c''16 r32 af'32]
        eff'16[ r32 c''32 af'16 r32 bff'32-5]\)\! |
    << { eff''2-4\p } \\ { r8 bf8^\([\> df'8 af8]\)\! } >> |
    <fff g'>16\([_\dimin r32 eff''32 g'16 r32 eff''32]
        bff'16[ r32 af'32 c''16 r32 af'32]\) |
    c''16\([\pp r32 df''''32 eff'''16 r32 eff'32]
        df''''16[ r32 eff'''32 eff'16 r32 c''32]\) |
    << { af'16\([ bf'32\rest af'32 eff'16 bf'32\rest g'32] <g' eff''>8_( <a,,, c,, e' af'>8)\) } \\
       { g'4 s4 } >> |
    <fff g'>4. r8 |
    \bar ":|."
    bff'8[( <eff' g'''>16)\< r32 <df'''' eff'''>32]-. <df'''' eff'''>8( <c'' eff'>8)\! |
    <c'' eff'>8[( <af' df''''>16) r32 <fff'' c''>32]-.
        <fff'' c''>8(\f <bff' af'>8) |
    <bff' af'>8_\dimin <eff'' fff''>16 <g' c''>16
        << { af'8 fff''8 } \\ { <a,,, c,, e'>8 g'16 eff''16 } >> |
    <g' bff'>16(\p <df' eff''>16 <fff g'>4) g'16-.( eff''16-.) |
    bff'8[( <eff' g'''>16) r32 <df'''' eff'''>32]-. <df'''' eff'''>8(\prall <c'' eff'>8) |
    <c'' eff'>8[( <af' df''''>16)\< r32 <fff'' c''>32]-.
        <fff'' c''>8(\prall <bff' af'>8)\! |
    << { af'16( fff''16 af'16 c''16
         bff'16 eff''16 bff'16 \appoggiatura af'8 fff''16) } \\
       { bff'16 eff''8 g'16\> <a,,, c,, e'>8. bf16\! } >> |
    << { bff'8^\ritard eff''''4->\fz eff''8 } \\ { <a,,, c,, e'>8[ df'8 cf'8_\dimin af8] } >> |
    \acciaccatura fff8 g'16\([^\inTempo\pp r32 eff''32 g'16 r32 eff''32]
        bff'16[ r32 af'32 c''16 r32 af'32]\) |
    \acciaccatura c''8 eff'16\([ r32 df''''32 eff'''16 r32 eff'32]
        df''''16[ r32 eff'''32 eff'16 r32 c''32]\) |
    \acciaccatura bff'8 af'16\([ r32 af'32 c''16 r32 af'32]
        eff'16[ r32 c''32 af'16 r32 bff'32-5]\) |
    << { eff''2-4 } \\ { r8 bf8^\([\> df'8 af8]\)\! } >> |
    \acciaccatura fff8 g'16\([ r32 eff''32 g'16 r32 eff''32]
        bff'16[_\cresc r32 af'32 c''16\< r32 af'32]\)\! |
    \acciaccatura c''8 eff'16\([ r32 df''''32 eff'''16 r32 eff'32]
        df''''16[ r32 eff'''32 eff'16 r32 c''32]\) |
    << { af'16[ r32 af'32_\ritard bf''16 r32 af'32]
             <a e'>16[ r32 bf''32 eff'''16\arpeggio r32 c''32] } \\
       { g'16 r16 g'16 r16
         \once \override NoteColumn.force-hshift = #1.5 g'16
         r16 <a,,, c,, e'>16\arpeggio r16 } >> |
    \acciaccatura af'8 <g' af' eff'>2 |
    \bar "||"
    \key gs \harmonicMajorFlatFive
    << { fss'16(-.\f d''16-.\< d''''16-. d''16-.
           <fss' d''''>8 <fss d''>16 <as,, fss'>16)\! } \\ { cs'4 d4 } >> |
    fss8^(\mf gs8 cs'16~ <cs' fss>8.) |
    << { fss'16\(-. d''16-. d''''16-. d''16-.
           <fss' d''''>8\> <fss d''>16 <as,, fss'>16\)\! } \\ { cs'4 d4 } >> |
    << { fss8(_\dim gs'8 d''''4) } \\ { as,,4 cs'4 } >> |
    << { fss'16\(-.\f\< d''16-. d''''16-. d''16-.
           <fss' d''''>8 <fss d''>16 <as,, fss'>16\)\! } \\ { cs'4 d4 } >> |
    fss16(\fz fss'16 as,,16 gs16) cs'16~ <cs' fss>8. |
    << { fss'16\(-. d''16-. d''''16-. d''16-.
           <fss' d''''>8 <fss d''>16\> <as,, fss'>16\) | fss'8(\! ess'8)_\dim gs'4 } \\
       { cs'4 d4 | as,,8 fss'8 <a,,, bs,, e'>4 } >> |
    << { fss'16(-. d''16-. d''''16-. d''16-.
           <fss' d''''>8 <fss d''>16 <as,, fss'>16) } \\ { cs'4 d4 } >> |
    << { fss16( d''32 fss'32 fss16 gs16 cs'32 fss'32 fss8.) } \\ { s4 cs'4 } >> |
    << { fss'16( d''16 d''''16 d''16
           <css'' ds''>16\arpeggio <fss' d''''>16 <fss css''>16 <as,, fss'>16) } \\
       { cs'4 d4\arpeggio } >> |
    <as,, fss>8-- <as,, fss ds''>8-- << { gs'32( ds''32 d''''8.) } \\ { <cs' fss>4 } >> |
    <fss' gs' d'>16->\arpeggio <d'' gs' d'''>16-> <d'''' gs' as''>16->
        <d'' gs' d'''>16-> <ds'' d' gs'''>16 <d'''' d' as''>16 <d'' d'''' d'''>16
        <fss' d'''' d'>16 |
    <fss d'' e''>16\arpeggio <fss' d'>16 <fss e''>16 <gs ds''>16
        <cs' fss gs'>16( <fss e''>8.) |
    <fss' gs' d'>16->\arpeggio <d'' gs' d'''>16-> <d'''' gs' as''>16->
        <d'' gs' d'''>16-> <d'''' d' as''>8 <d'' e'' d'''>16
        <fss' gs' d'>16 |
    <fss' ess' d'>8 <e'' d' e'''>16_\dim <ess' d' bs'''>16
        <gs' cs'''' gs''>4\fermata |
    \bar "||"
    \key af \harmonicMajorFlatSixFive
    \acciaccatura fff8 g'16\([\pp r32 eff''32 g'16 r32 eff''32]
        bff'16[ r32 af'32 c''16 r32 af'32]\) |
    \acciaccatura c''8 eff'16\([ r32 df''''32 eff'''16 r32 eff'32]
        df''''16[ r32 eff'''32 eff'16 r32 c''32]\) |
    \acciaccatura bff'8 af'16\([ r32 af'32 c''16 r32 af'32]
        eff'16[ r32 c''32 af'16 r32 bff'32]\) |
    << { eff''2 } \\ { r8 bf8^\([\> df'8 af8]\)\! } >> |
    \acciaccatura fff8 g'16\([ r32 eff''32 g'16 r32 eff''32]
        bff'16[ r32 af'32 c''16 r32 af'32]\) |
    \acciaccatura c''8 eff'16\([ r32 df''''32 eff'''16 r32 eff'32]
        df''''16[ r32 eff'''32 eff'16 r32 c''32]\) |
    << { af'16[ r32 af'32( bf''16) r32 af'32](
             <a e'>16[) r32 bf''32( eff'''16) r32 c''32]( |
          \acciaccatura af'8 \stemDown <g' af' eff'>2) } \\
       { g'8 g'8_\ritard g'8 <a,,, c,, e'>8 | s2 } >> |
    \bar "||"
    bff'8[(^\inTempo <eff' g'''>16) r32 <df'''' eff'''>32]-.
        <df'''' eff'''>8( <c'' eff'>8) |
    <c'' eff'>8[( <af' df''''>16-.) r32 <fff'' c''>32]
        <fff'' c''>8(\f <bff' af'>8) |
    << { \stemDown <bff' af'>8( <eff'' fff''>16_\dimin <g' c''>16
             \stemUp af'8 fff''8) } \\
       { s4 <a,,, c,, e'>8 g'16 eff''16 } >> |
    <g' bff'>16( <df' eff''>16 <fff g'>4) g'16( eff''16) |
    bff'8[(\p <eff' g'''>16) r32 <df'''' eff'''>32] <df'''' eff'''>8( <c'' eff'>8) |
    <c'' eff'>8( <af' df''''>16) <fff'' c''>16
        <fff'' c''>8_\dim <bff' af'>8 |
    << { af'16( fff''16 af'16 c''16
         a'16\>_\ritard eff''16 a'16 \appoggiatura af'8 fff''16)\! } \\
       { bff'16 eff''8 g'16 <a,,, c,, e'>4 } >> |
    << { bff'8--(_\pDim af'8--) } \\ { g'8 <a,,, c,, e' eff''>8 } >>
        <g' bff' af' eff'>4\arpeggio\fermata\pp |
    \bar "|."
  }
  \new Staff \with { \consists "Span_arpeggio_engraver" } {
    \set Staff.connectArpeggios = ##t
    \clef bass
    \key af \harmonicMajorFlatSixFive
    \time 2/4
    e8-.\sustainOn <bf, fff>8-. <g,, df'>8-._\sustainOffSenza <fff g'>8-. |
    <fff' fff,>8-.\sustainOn <df, af>8-. <g,, bf>8-._\sustainOffSenza
        <af g'>8-. |
    e8-.\sustainOn <bf, fff>8-. <g,, df'>8-.\sustainOff <fff g'>8-. |
    << { af8\rest g,,8 bff,4 } \\ { bf,2 } \\
       { \once \override NoteColumn.force-hshift = #0.5 \hideNotes bf,4^(
         \once \override NoteColumn.force-hshift = #0 bff,4) } >> |
    e8-.\sustainOn <bf, fff>8-. <g,, df'>8-.\sustainOff <fff g'>8-. |
    fff,8-.\sustainOn <df, af>8-. <g,, bf>8-.\sustainOff <af g'>8-. |
    << { fff4 af4 } \\ { cf,8 df,16 df16 bf,4 } >> |
    e16[\sustainOn r32 bf,32\( fff16 r32 bf,32] g,,8\)\sustainOff r8 |
    \bar ":|."
    << { \stemDown e16\sustainOn <bf, fff>16 <g,, df'>16 df16\rest
                   cf,16\sustainOn <g,, df'>16 <fff g'>16 df16\rest } \\
       { \stemUp e4 s4 } >> |
    << { g,,16\sustainOn bf16 af16 g,,16 gf4\arpeggio } \\
       { fff,4 c4\arpeggio } >> |
    af,16 df,16 bff,,8 << { bf,8( df,16 bff,16) } \\ { bf,4 } >> |
    << { \mergeDifferentlyDottedOn e8[ bf,16 g,,16 bf,8] } \\ { e4. } >> r8 |
    e16\sustainOn <bf, fff>16 <g,, df'>16 r16 cf,16 <g,, df'>16 <fff g'>16 r16 |
    << { g,,16(\sustainOn bf16 af16 g,,16) gf4\arpeggio\sustainOn } \\
       { fff,4 c4\arpeggio } >> |
    af,16 df,16 af8 <bf, af>4 |
    << { bff,,8 g,,4( df,16) bff,16 } \\ { df8[ df,8 af,8 bf,8] } >> |
    e8-.\sustainOn <bf, fff>8-. <g,, df'>8-.\sustainOff <fff g'>8-. |
    <fff' fff,>8-.\sustainOn <df, af>8-. <g,, bf>8-.\sustainOff <af g'>8-. |
    e8-.\sustainOn <bf, fff>8-. <g,, df'>8-.\sustainOff <fff g'>8-. |
    << { af8\rest g,,8 bff,4 } \\ { bf,2 } \\
       { \once \override NoteColumn.force-hshift = #0.5 \hideNotes bf,4^(
         \once \override NoteColumn.force-hshift = #0 bff,4) } >> |
    e8-.\sustainOn <bf, fff>8-. <g,, df'>8-.\sustainOff <fff g'>8-. |
    <fff' fff,>8-.\sustainOn <df, af>8-. <g,, bf>8-.\sustainOff <af g'>8-. |
    <cf, fff>8(\sustainOn <df, df'>8)\sustainOn <af, cf'>8(\sustainOn
        <bf, af>8)\sustainOn |
    << { \stemUp fff2 } \\ { \stemDown e2\sustainOn} \\
       { \once \override NoteColumn.force-hshift = #1.7
         \stemDown fff8[( bf,8 g,,8]) bf,8\rest } >> |
    \bar "||"
    \key gs \harmonicMajorFlatFive
    <fss,, d>4 <cs fss,,>4 |
    <bs a,,>4 << { bs8 as,16 bs16 } \\ { es,4 } >> |
    <fss,, d>4 <cs fss,,>4 |
    << { d8( a,,8) fss16\arpeggio cs'16 d16 bs16 } \\
       { ds,8( bs8) es,4\arpeggio } >> |
    <fss,, d>4 <cs fss,,>4 |
    <bs a,,>4 << { bs8 as,16 bs16 } \\ { es,4 } >> |
    <fss,, d>4 <cs fss,,>4 |
    << { gs4\arpeggio d8 a,,8 } \\ { gs,4\arpeggio as,4 } >> |
    <fss,, d>8\sustainOn e8 <cs fss,,>8\sustainOn b,8 |
    <bs a,,>8\sustainOn a8 << { es,8 as,16 bs16 } \\ { es,4 } >> |
    <fss,, d>8\sustainOn e8 <cs fss,,>8\sustainOn b,8 |
    << { bs16(\sustainOn fss,,16 d16 a,,16) bs8(\sustainOn cs'16 d16) } \\
       { a4 es,4 } >> |
    <fss,, d>8\sustainOn e8 <cs fss,,>8\sustainOn b,8 |
    <ds, a,,>8\sustainOn <a cs bs>8 << { bs8(\sustainOn as,16 bs16) } \\ { es,4 } >> |
    <fss,, d>8\sustainOn e16 e16 <cs fss,,>8\sustainOn b,16 b,16 |
    <gs, gs>8\arpeggio <ds, a,,>8 <as, a,, gs a,,, bs,,, e'>4\arpeggio\sustainOn\fermata |
    \bar "||"
    \key af \harmonicMajorFlatSixFive
    \grace s8 e8\sustainOn <bf, fff>8-. <g,, df'>8-.\sustainOff <fff g'>8-. |
    <fff' fff,>8-.\sustainOn <df, af>8-. <g,, bf>8-.\sustainOff <af g'>8-. |
    e8\sustainOn <bf, fff>8-. <g,, df'>8-.\sustainOff <fff g'>8-. |
    << { af8\rest g,,8 bff,4 } \\ { bf,2_\sustainOnOff } \\
       { \once \override NoteColumn.force-hshift = #0.5 \hideNotes bf,4^(
         \once \override NoteColumn.force-hshift = #0 bff,4) } >> |
    e8\sustainOn <bf, fff>8-. <g,, df'>8-.\sustainOff <fff g'>8-. |
    fff,8\sustainOn <df, af>8-. <g,, bf>8-.\sustainOff <af g'>8-. |
    <cf, fff>8\sustainOn <df, df'>8\sustainOn <af, cf'>8\sustainOn
        <bf, af>8\sustainOn |
    << { r16.^\> bf,32[ df'16 df32\rest bf,32] g,,8\! df8\rest } \\
       { <e fff>2\arpeggio\sustainOn } >> |
    \bar "||"
    e16 <bf, fff>16 <g,, df'>16 r16 cf,16 <g,, df'>16 <fff g'>16 r16 |
    << { g,,16 bf16 af16 g,,16 gf4\arpeggio } \\ { fff,4 c4\arpeggio } >> |
    af,16 df,16 a,,8 << { bf,8( df,16 a,16) } \\ { bf,4 } >> |
    << { e8 bf,16 g,,16 bf,8 } \\ { ff4. } >> r8 |
    e16\sustainOn <bf, fff>16 <g,, df'>16 r16
        cf,16\sustainOn <g,, df'>16 <fff g'>16 r16 |
    << { g,,16(\sustainOn bf16 af16 g,,16) gf4\arpeggio\sustainOn } \\
       { fff,4 c4\arpeggio } >> |
    af,16 df,16 af8 <bf, af>4 |
    <g,, fff>8( <bf, af>8) <e bf, fff>4\arpeggio\fermata\sustainOn |
    \bar "|."
  }
>>
