#!/usr/bin/perl

use strict;
use warnings;

my @dvorak = (
  qw(! 7 5 3 1 9 0 2 4 6 8 = ?), ',',
  qw(. p y f g c r l / a o e u i d h t n s -), "'", qw(q j k x b m w v z ¼),
  '#', '(', ')', '"', qw(% _ ¢ $ @ * + :), ',',
  qw(. P Y F G C R L & A O E U I D H T N S ½ ; Q J K X B M W V Z)
);
scalar @dvorak == 88 or die "Wrong number of Dvorak keys";

my @qwerty = (
  qw(1 2 3 4 5 6 7 8 9 0 - = q w e r t y u i o p [ a s d f g h j k l ;), "'",
  qw(z x c v b n m), ',', qw(. / ! @), '#', qw($ % ^ & *), '(', ')',
  qw(_ + Q W E R T Y U I O P { A S D F G H J K L :), '"',
  qw(Z X C V B N M < > ?)
);
scalar @qwerty == 88 or die "Wrong number of QWERTY keys";

my %qwerty_to_offset = map { ($qwerty[$_], $_) } (0..87);

my @pitch_count = map { 0 } (0..11);

while (my $line = <>) {
  $line =~ s/Dvořák/Qweřtý/g;
  $line = protect_line($line);
  $line =~ s/(?:^|(?<=[^\pL\\]))  # start of line or non-letter, non-backslash
             ([abcdefg](?:s|f|ss|x|ff)?[,']*)  # pitch
             (?:$|(?=[^\pL]))  # end of line or non-letter
            /qwertify($1)/egx;
  $line = unprotect_line($line);
  print $line;
}

print STDERR "Pitch counts:";
for (my $i = 0; $i < 12; ++$i) {
  print STDERR " ", qw(c cs d ds e f fs g gs a as b)[$i], ":$pitch_count[$i]";
}
print STDERR "\n";

sub protect_line {
  my $line = shift;
  $line =~ s/(\\key\s+)([abcdefg][sf]?)/$1.protect($2)/eg;  # key signature
  $line =~ s/"([^"]*)"/'"'.protect($1).'"'/eg;  # string literal
  return $line;
}

sub protect {
  my $string = shift;
  $string =~ s/([abcdefg])/'@@'.uc($1)/eg;
  return 'QWERTIFY_PROTECT_BEGIN'.$string.'QWERTIFY_PROTECT_END';
}

sub unprotect_line {
  my $line = shift;
  $line =~ s/QWERTIFY_PROTECT_BEGIN(.*?)QWERTIFY_PROTECT_END/unprotect($1)/eg;
  return $line;
}

sub unprotect {
  my $string = shift;
  $string =~ s/@@([ABCDEFG])/lc($1)/eg;
  return $string;
}

sub qwertify {
  my $pitch = shift;
  $pitch =~ /^([abcdefg])(s|f|ss|x|ff)?([,']*)$/ or die "wtf: $pitch";
  my ($base, $accidental, $octave) = ($1, $2, $3);
  my %base_to_offset = (
    c => 27, d => 29, e => 31, f => 32, g => 34, a => 36, b => 38
  );
  my $offset = $base_to_offset{$base};
  if (defined $accidental) {
    $offset += { s => +1, f => -1, ss => +2, x => +2, ff => -2 }->{$accidental};
  }
  if ($octave ne '') {
    if ($octave =~ /^,+$/) {
      $offset -= 12 * length $octave;
    } elsif ($octave =~ /^'+$/) {
      $offset += 12 * length $octave;
    } else {
      die "wtf: $pitch";
    }
  }
  0 <= $offset && $offset < 88 or die "Pitch out of range: $pitch";
  my $character = $dvorak[$offset];
  if (exists $qwerty_to_offset{$character}) {
    my $new_offset = $qwerty_to_offset{$character};
    my $new_octave = '';
    while ($new_offset < 27) {
      $new_octave .= ',';
      $new_offset += 12;
    }
    while ($new_offset > 38) {
      $new_octave .= "'";
      $new_offset -= 12;
    }
    ++$pitch_count[$new_offset - 27];
    my %offset_to_base = map { ($base_to_offset{$_}, $_) } keys %base_to_offset;
    my $new_accidental = '';
    if (!exists $offset_to_base{$new_offset}) {
      if (!defined $accidental || $accidental =~ /^f/) {
        $new_accidental = 'f';
        ++$new_offset;
      } else {
        $new_accidental = 's';
        --$new_offset;
      }
    }
    return $offset_to_base{$new_offset}.$new_accidental.$new_octave;
  } else {
    return "\\xNote $pitch";
  }
}
