\version "2.18.2"
\language "english"

\header {
  title = "Humoresque No. 7 in G-flat Major"
  composer = "Antonín Dvořák"
  tagline = ""
}

leggiero = \tweak DynamicText.self-alignment-X #LEFT
           #(make-dynamic-script (markup #:normal-text #:italic "leggiero"))
sustainOffSenza = \markup { \musicglyph #"pedal.*" \italic senza }
sustainOnOff = \markup {
                 \concat { \musicglyph #"pedal.Ped" \musicglyph #"pedal.." }
                 \musicglyph #"pedal.*"
               }
dimin = \markup { \italic "dimin." }
ritard = \markup { \italic "ritard." }
inTempo = \markup { \italic "in tempo" }
cresc = \markup { \italic "cresc." }
dim = \markup { \italic "dim." }
pDim = \markup { \dynamic p \italic "dim." }

\new GrandStaff <<
  \new Staff \with { \consists "Span_arpeggio_engraver" } {
    \set Staff.connectArpeggios = ##t
    \tempo "Poco Lento e grazioso."
    \clef treble
    \key gf \major
    \time 2/4
    gf'16\([_\leggiero r32 af'32 gf'16 r32 af'32]
        bf'16[ r32 df''32 ef''16\< r32 df''32]\) |
    \acciaccatura ef''8 gf''16\([ r32 f''32 af''16\! r32 gf''32]
        f''16[ r32 af''32 gf''16 r32 ef''32]\) |
    \acciaccatura bf'8 df''16\([\> r32 df''32 ef''16 r32 df''32]
        gf''16[ r32 ef''32 df''16 r32 bf'32-5]\)\! |
    << { af'2-4\p } \\ { r8 ef'8^\([\> df'8 cf'8]\)\! } >> |
    <bf gf'>16\([_\dimin r32 af'32 gf'16 r32 af'32]
        bf'16[ r32 df''32 ef''16 r32 df''32]\) |
    ef''16\([\pp r32 f''32 af''16 r32 gf''32]
        f''16[ r32 af''32 gf''16 r32 ef''32]\) |
    << { df''16\([ r32 df''32 gf''16 r32 gf'32] <gf' af'>8_( <f' df''>8)\) } \\
       { gf'4 s4 } >> |
    <bf gf'>4. r8 |
    \bar ":|."
    bf'8[( <gf'' bf''>16)\< r32 <f'' af''>32]-. <f'' af''>8( <ef'' gf''>8)\! |
    <ef'' gf''>8[( <df'' f''>16) r32 <cf'' ef''>32]-.
        <cf'' ef''>8(\f <bf' df''>8) |
    <bf' df''>8_\dimin <af' cf''>16 <gf' ef''>16
        << { df''8 cf''8 } \\ { f'8 gf'16 af'16 } >> |
    <gf' bf'>16(\p <df' af'>16 <bf gf'>4) gf'16-.( af'16-.) |
    bf'8[( <gf'' bf''>16) r32 <f'' af''>32]-. <f'' af''>8(\prall <ef'' gf''>8) |
    <ef'' gf''>8[( <df'' f''>16)\< r32 <cf'' ef''>32]-.
        <cf'' ef''>8(\prall <bf' df''>8)\! |
    << { df''16( cf''16 df''16 ef''16
         bf'16 af'16 bf'16 \appoggiatura df''8 cf''16) } \\
       { bf'16 af'8 gf'16\> f'8. ef'16\! } >> |
    << { bf'8^\ritard bff'4->\fz af'8 } \\ { f'8[ df'8 c'8_\dimin cf'8] } >> |
    \acciaccatura bf8 gf'16\([^\inTempo\pp r32 af'32 gf'16 r32 af'32]
        bf'16[ r32 df''32 ef''16 r32 df''32]\) |
    \acciaccatura ef''8 gf''16\([ r32 f''32 af''16 r32 gf''32]
        f''16[ r32 af''32 gf''16 r32 ef''32]\) |
    \acciaccatura bf'8 df''16\([ r32 df''32 ef''16 r32 df''32]
        gf''16[ r32 ef''32 df''16 r32 bf'32-5]\) |
    << { af'2-4 } \\ { r8 ef'8^\([\> df'8 cf'8]\)\! } >> |
    \acciaccatura bf8 gf'16\([ r32 af'32 gf'16 r32 af'32]
        bf'16[_\cresc r32 df''32 ef''16\< r32 df''32]\)\! |
    \acciaccatura ef''8 gf''16\([ r32 f''32 af''16 r32 gf''32]
        f''16[ r32 af''32 gf''16 r32 ef''32]\) |
    << { df''16[ r32 df''32_\ritard bff''16 r32 df''32]
             c''16[ r32 bff''32 af''16\arpeggio r32 ef''32] } \\
       { gf'16 r16 gf'16 r16 gf'16 r16 f'16\arpeggio r16 } >> |
    \acciaccatura df''8 <gf' df'' gf''>2 |
    \bar "||"
    \key fs \minor
    << { fs'16(-.\f gs'16-.\< a'16-. gs'16-.
           <fs' a'>8 <e' gs'>16 <d' fs'>16)\! } \\ { cs'4 a4 } >> |
    e'8^(\mf b8 cs'16~ <cs' e'>8.) |
    << { fs'16\(-. gs'16-. a'16-. gs'16-.
           <fs' a'>8\> <e' gs'>16 <d' fs'>16\)\! } \\ { cs'4 a4 } >> |
    << { e'8(_\dim cs''8 a'4) } \\ { d'4 cs'4 } >> |
    << { fs'16\(-.\f\< gs'16-. a'16-. gs'16-.
           <fs' a'>8 <e' gs'>16 <d' fs'>16\)\! } \\ { cs'4 a4 } >> |
    e'16(\fz fs'16 d'16 b16) cs'16~ <cs' e'>8. |
    << { fs'16\(-. gs'16-. a'16-. gs'16-.
           <fs' a'>8 <e' gs'>16\> <d' fs'>16\) | fs'8(\! d''8)_\dim cs''4 } \\
       { cs'4 a4 | d'8 fs'8 es'4 } >> |
    << { fs'16(-. gs'16-. a'16-. gs'16-.
           <fs' a'>8 <e' gs'>16 <d' fs'>16) } \\ { cs'4 a4 } >> |
    << { e'16( gs'32 fs'32 e'16 b16 cs'32 fs'32 e'8.) } \\ { s4 cs'4 } >> |
    << { fs'16( gs'16 a'16 gs'16
           <gs' b'>16\arpeggio <fs' a'>16 <e' gs'>16 <d' fs'>16) } \\
       { cs'4 a4\arpeggio } >> |
    <d' e'>8-- <d' e' b'>8-- << { cs''32( b'32 a'8.) } \\ { <cs' e'>4 } >> |
    <fs' cs'' fs''>16->\arpeggio <gs' cs'' gs''>16-> <a' cs'' a''>16->
        <gs' cs'' gs''>16-> <b' fs'' b''>16 <a' fs'' a''>16 <gs' a' gs''>16
        <fs' a' fs''>16 |
    <e' gs' e''>16\arpeggio <fs' fs''>16 <e' e''>16 <b b'>16
        <cs' e' cs''>16( <e' e''>8.) |
    <fs' cs'' fs''>16->\arpeggio <gs' cs'' gs''>16-> <a' cs'' a''>16->
        <gs' cs'' gs''>16-> <a' fs'' a''>8 <gs' e'' gs''>16
        <fs' cs'' fs''>16 |
    <fs' d'' fs''>8 <e'' fs'' e'''>16_\dim <d'' fs'' d'''>16
        <cs'' es'' cs'''>4\fermata |
    \bar "||"
    \key gf \major
    \acciaccatura bf8 gf'16\([\pp r32 af'32 gf'16 r32 af'32]
        bf'16[ r32 df''32 ef''16 r32 df''32]\) |
    \acciaccatura ef''8 gf''16\([ r32 f''32 af''16 r32 gf''32]
        f''16[ r32 af''32 gf''16 r32 ef''32]\) |
    \acciaccatura bf'8 df''16\([ r32 df''32 ef''16 r32 df''32]
        gf''16[ r32 ef''32 df''16 r32 bf'32]\) |
    << { af'2 } \\ { r8 ef'8^\([\> df'8 cf'8]\)\! } >> |
    \acciaccatura bf8 gf'16\([ r32 af'32 gf'16 r32 af'32]
        bf'16[ r32 df''32 ef''16 r32 df''32]\) |
    \acciaccatura ef''8 gf''16\([ r32 f''32 af''16 r32 gf''32]
        f''16[ r32 af''32 gf''16 r32 ef''32]\) |
    << { df''16[ r32 df''32( bff''16) r32 df''32](
             c''16[) r32 bff''32( af''16) r32 ef''32]( |
          \acciaccatura df''8 \stemDown <gf' df'' gf''>2) } \\
       { gf'8 gf'8_\ritard gf'8 f'8 | s2 } >> |
    \bar "||"
    bf'8[(^\inTempo <gf'' bf''>16) r32 <f'' af''>32]-.
        <f'' af''>8( <ef'' gf''>8) |
    <ef'' gf''>8[( <df'' f''>16-.) r32 <cf'' ef''>32]
        <cf'' ef''>8(\f <bf' df''>8) |
    << { \stemDown <bf' df''>8( <af' cf''>16_\dimin <gf' ef''>16
             \stemUp df''8 cf''8) } \\
       { s4 f'8 gf'16 af'16 } >> |
    <gf' bf'>16( <df' af'>16 <bf gf'>4) gf'16( af'16) |
    bf'8[(\p <gf'' bf''>16) r32 <f'' af''>32] <f'' af''>8( <ef'' gf''>8) |
    <ef'' gf''>8( <df'' f''>16) <cf'' ef''>16
        <cf'' ef''>8_\dim <bf' df''>8 |
    << { df''16( cf''16 df''16 ef''16
         bf'16\>_\ritard af'16 bf'16 \appoggiatura df''8 cf''16)\! } \\
       { bf'16 af'8 gf'16 f'4 } >> |
    << { bf'8--(_\pDim df''8--) } \\ { gf'8 <f' af'>8 } >>
        <gf' bf' df'' gf''>4\arpeggio\fermata\pp |
    \bar "|."
  }
  \new Staff \with { \consists "Span_arpeggio_engraver" } {
    \set Staff.connectArpeggios = ##t
    \clef bass
    \key gf \major
    \time 2/4
    gf,8-.\sustainOn <df bf>8-. <gf df'>8-._\sustainOffSenza <bf gf'>8-. |
    <cf, cf>8-.\sustainOn <ef cf'>8-. <gf ef'>8-._\sustainOffSenza
        <cf' gf'>8-. |
    gf,8-.\sustainOn <df bf>8-. <gf df'>8-.\sustainOff <bf gf'>8-. |
    << { cf'8\rest gf8 f4 } \\ { df2 } \\
       { \once \override NoteColumn.force-hshift = #0.5 \hideNotes df4_(
         \once \override NoteColumn.force-hshift = #0 f4) } >> |
    gf,8-.\sustainOn <df bf>8-. <gf df'>8-.\sustainOff <bf gf'>8-. |
    cf8-.\sustainOn <ef cf'>8-. <gf ef'>8-.\sustainOff <cf' gf'>8-. |
    << { bf4 cf'4 } \\ { bf,8 ef16 eff16 df4 } >> |
    gf,16[\sustainOn r32 df32\( bf16 r32 df32] gf8\)\sustainOff r8 |
    \bar ":|."
    << { \stemDown gf,16\sustainOn <df bf>16 <gf df'>16 df16\rest
                   bf,16\sustainOn <gf df'>16 <bf gf'>16 df16\rest } \\
       { \stemUp gf,4 s4 } >> |
    << { gf16\sustainOn ef'16 cf'16 gf16 g4\arpeggio } \\
       { cf4 ef,4\arpeggio } >> |
    af,16 ef16 af8 << { df8( ef16 f16) } \\ { df4 } >> |
    << { \mergeDifferentlyDottedOn gf,8[ df16 gf16 df8] } \\ { gf,4. } >> r8 |
    gf,16\sustainOn <df bf>16 <gf df'>16 r16 bf,16 <gf df'>16 <bf gf'>16 r16 |
    << { gf16(\sustainOn ef'16 cf'16 gf16) g4\arpeggio\sustainOn } \\
       { cf4 ef,4\arpeggio } >> |
    af,16 ef16 cf'8 <df cf'>4 |
    << { af8 gf4( ef16) f16 } \\ { d8[ ef8 af,8 df8] } >> |
    gf,8-.\sustainOn <df bf>8-. <gf df'>8-.\sustainOff <bf gf'>8-. |
    <cf, cf>8-.\sustainOn <ef cf'>8-. <gf ef'>8-.\sustainOff <cf' gf'>8-. |
    gf,8-.\sustainOn <df bf>8-. <gf df'>8-.\sustainOff <bf gf'>8-. |
    << { cf'8\rest gf8 f4 } \\ { df2 } \\
       { \once \override NoteColumn.force-hshift = #0.5 \hideNotes df4_(
         \once \override NoteColumn.force-hshift = #0 f4) } >> |
    gf,8-.\sustainOn <df bf>8-. <gf df'>8-.\sustainOff <bf gf'>8-. |
    <cf, cf>8-.\sustainOn <ef cf'>8-. <gf ef'>8-.\sustainOff <cf' gf'>8-. |
    <bf, bf>8(\sustainOn <ef df'>8)\sustainOn <af, c'>8(\sustainOn
        <df cf'>8)\sustainOn |
    << { \stemUp bf2 } \\ { \stemDown gf,2\sustainOn} \\
       { \once \override NoteColumn.force-hshift = #1.2
         \stemDown bf8[( df8 gf8]) df8\rest } >> |
    \bar "||"
    \key fs \minor
    <fs a>4 <d fs>4 |
    <e gs>4 << { e8 cs16 e16 } \\ { a,4 } >> |
    <fs a>4 <d fs>4 |
    << { a8( gs8) e'16\arpeggio cs'16 a16 e16 } \\
       { b,8( e8) a,4\arpeggio } >> |
    <fs a>4 <d fs>4 |
    <e gs>4 << { e8 cs16 e16 } \\ { a,4 } >> |
    <fs a>4 <d fs>4 |
    << { b4\arpeggio a8 gs8 } \\ { gs,4\arpeggio cs4 } >> |
    <fs a>8\sustainOn fs,8 <d fs>8\sustainOn d,8 |
    <e gs>8\sustainOn e,8 << { a,8 cs16 e16 } \\ { a,4 } >> |
    <fs a>8\sustainOn fs,8 <d fs>8\sustainOn d,8 |
    << { e16(\sustainOn fs16 a16 gs16) e8(\sustainOn cs'16 a16) } \\
       { e,4 a,4 } >> |
    <fs a>8\sustainOn fs,8 <d fs>8\sustainOn d,8 |
    <b, gs>8\sustainOn <e, d e>8 << { e8(\sustainOn cs16 e16) } \\ { a,4 } >> |
    <fs a>8\sustainOn fs,16 fs,16 <d fs>8\sustainOn d,16 d,16 |
    <gs, b>8\arpeggio <b, gs>8 <cs gs b es'>4\arpeggio\sustainOn\fermata |
    \bar "||"
    \key gf \major
    \grace s8 gf,8\sustainOn <df bf>8-. <gf df'>8-.\sustainOff <bf gf'>8-. |
    <cf, cf>8-.\sustainOn <ef cf'>8-. <gf ef'>8-.\sustainOff <cf' gf'>8-. |
    gf,8\sustainOn <df bf>8-. <gf df'>8-.\sustainOff <bf gf'>8-. |
    << { cf'8\rest gf8 f4 } \\ { df2_\sustainOnOff } \\
       { \once \override NoteColumn.force-hshift = #0.5 \hideNotes df4_(
         \once \override NoteColumn.force-hshift = #0 f4) } >> |
    gf,8\sustainOn <df bf>8-. <gf df'>8-.\sustainOff <bf gf'>8-. |
    cf8\sustainOn <ef cf'>8-. <gf ef'>8-.\sustainOff <cf' gf'>8-. |
    <bf, bf>8\sustainOn <ef df'>8\sustainOn <af, c'>8\sustainOn
        <df cf'>8\sustainOn |
    << { r16.^\> df32[ df'16 af32\rest df32] gf8\! af8\rest } \\
       { <gf, bf>2\arpeggio\sustainOn } >> |
    \bar "||"
    gf,16 <df bf>16 <gf df'>16 r16 bf,16 <gf df'>16 <bf gf'>16 r16 |
    << { gf16 ef'16 cf'16 gf16 g4\arpeggio } \\ { cf4 ef,4\arpeggio } >> |
    af,16 ef16 af8 << { df8( ef16 f16) } \\ { df4 } >> |
    << { gf,8 df16 gf16 df8 } \\ { gf,4. } >> r8 |
    gf,16\sustainOn <df bf>16 <gf df'>16 r16
        bf,16\sustainOn <gf df'>16 <bf gf'>16 r16 |
    << { gf16(\sustainOn ef'16 cf'16 gf16) g4\arpeggio\sustainOn } \\
       { cf4 ef,4\arpeggio } >> |
    af,16 ef16 cf'8 <df cf'>4 |
    <gf bf>8( <df cf'>8) <gf, df bf>4\arpeggio\fermata\sustainOn |
    \bar "|."
  }
>>
