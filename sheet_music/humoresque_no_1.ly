\version "2.18.2"
\language "english"

\header {
  title = "Humoresque No. 1 in E-flat Minor"
  composer = "Antonín Dvořák"
  tagline = ""
}

conPed = \markup { \italic con
                   \concat { \musicglyph #"pedal.Ped" \musicglyph #"pedal.." } }
dolce = \markup { \italic dolce }
conDolore = \markup { \italic "con dolore" }
espressivo = \markup { \italic espressivo }
marcato = \markup { \italic marcato }
crescMoltoEdAccel = \tweak DynamicText.self-alignment-X #LEFT
                    #(make-dynamic-script (markup #:normal-text #:italic
                        "cresc. molto ed accel."))

\new GrandStaff <<
  \new Staff = "right" {
    \tempo "Vivace" 2 = 72
    \clef treble
    \key ef \minor
    \time 2/4
    <ef'' ef'''>4.-^ \tuplet 3/2 { df'''16\(( ef''' df''') } |
    <bf' bf''>2\)-^ |
    <af' af''>4.-^ \tuplet 3/2 { gf''16\(( af'' gf'') } |
    <ef' ef''>2\)-^ |
    <ef' ef''>8[-. <f' f''>-. <gf' gf''>-. <af' af''>]-. |
    <gf' gf''>4-. <ef' ef''>-. |
    <ef' ef''>2~-^ |
    <ef' ef''>2 |
    << { ef''8[-. f''-. gf''-. af'']-. } \\ { cf''4. af'8 } >> |
    << { gf''4 ef'' } \\ { bf'4 cf'' } >> |
    << { gf''4 ef'' } \\ { bf'4 \tuplet 3/2 { cf''8( bf' af') } } >> |
    << { ef''2 } \\ { \afterGrace gf'2(\trill { \stemUp f'16 gf') } } >> |
    << { ef''8[-. f''-. gf''-. af''-.] } \\ { cf''8[ df'' bf' cf''] } >> |
    << { gf''4 ef'' } \\ { cf''8 bf' a' bf' } >> |
    << { gf''4 f''8 ef'' } \\ { cf''!8 ef'' df'' cf'' } >> |
    << { f''2 } \\ { cf''!8[ bf' a' bf'] } >> |
    <ef'' ef'''>4. \tuplet 3/2 { df'''!16\(( ef''' df''') } |
    <bf' bf''>2\) |
    <af' af''>4. \tuplet 3/2 { gf''16\(( af'' gf'') } |
    <ef' ef''>2\) |
    <ef' ef''>8[-. <f' f''>-. <gf' gf''>-. <af' af''>]-. |
    <gf' gf''>4-. <ef' ef''>-. |
    <ef' ef''>2~-^ |
    <ef' ef''>2
    \bar "||"
    << { ef'8[^\dolce f' g' af'] } \\ { bf2 } >> |
    <bf ef' g'>4^( <bf ef'>) |
    <af ef' f'>4^( <c' ef' af'>) |
    <bf d' g'>4^( <g ef'>) |
    << { ef'8\([-- f'-- g'-- af']-- | g'4 ef'\) } \\ { bf2 | ef'2 } >> |
    << { <a ef' g'>4^( \once \override NoteColumn.force-hshift = #-1.9 <af f'> |
         <g ef'>2) } \\ { s4 ef'8 d' | s2 } >> |
    ef''8[-. f''-. g''-. af'']-. |
    g''8-. g''-. r ef''( |
    f''8.[ ef''16 d''8. c''16]) |
    bf'8( ef''4.)\fz |
    ef''8[-. f''-. g''-. af'']-. |
    g''8-. g''-. r ef'' |
    << { s4 bf'8\( g'' | f''8( ef''4.\)) } \\ { f''8. c''16 bf'4~ | bf'2 } >> |
    << { ef'8[^\dolce f' g' af'] } \\ { bf2 } >> |
    <bf ef' g'>4^( <bf ef'>) |
    <af ef' f'>4^( <c' ef' af'>) |
    <bf d' g'>4^( <g ef'>) |
    << { ef'8\([-- f'-- g'-- af']-- | g'4 ef'\) } \\ { bf2 | ef'2 } >> |
    << { <a ef' g'>4^( \once \override NoteColumn.force-hshift = #-1.9 <af f'> |
         <g ef'>2) } \\ { s4 ef'8 d' | s2 } >> |
    ef''8[-. f''-. g''-. af'']-. |
    g''8-. g''-. r ef''( |
    f''8.[ ef''16 d''8. c''16]) |
    bf'8( ef''4.)\fz |
    ef''8[-. f''-. g''-. af'']-. |
    g''8-. g''-. r ef'' |
    << { s4 bf'8\( g'' | f''8( ef''4.\)) } \\ { f''8. c''16 bf'4~ | bf'2 } >>
    \bar "||"
    <ef'' ef'''>4.-^ \tuplet 3/2 { df'''16\(( ef''' df''') } |
    <bf' bf''>2\) |
    <af' af''>4. \tuplet 3/2 { gf''16\(( af'' gf'') } |
    <ef' ef''>2\) |
    <ef' ef''>8[-. <f' f''>-. <gf' gf''>-. <af' af''>]-. |
    <gf' gf''>4-. <ef' ef''>-. |
    <ef' ef''>2~-^ |
    <ef' ef''>2
    \bar "||"
    << { gf'8[-. af'-. bf'-. cf'']-. } \\ { df'2 } >> |
    << { <df' gf' bf'>4\( <df' gf'> | af'4 bf'\) } \\
       { s2 | <cf' gf'>4 <df' f'> } >> |
    << { s2 | gf'8[-. af'-. bf'-. cf'']-. } \\
       { \stemUp <bf df' gf'>8^\( ef' df'4\)->~ | \stemDown df'2 } >> |
    % http://lists.gnu.org/archive/html/lilypond-user/2018-02/msg00302.html
    \voiceOne <df' gf' bf'>4\( <df' gf'> |
    <c' gf' bf'>4 <<
                    { \voiceTwo gf'8 f'\) }
                    \new Voice { \voiceOne af'4 }
                    \new Voice { \voiceFour cf'4 }
                  >> |
    \oneVoice <bf gf'>2 |
    gf''8[-. af''-. bf''-. cf''']-. |
    bf''8-. bf''-. r gf'' |
    bf''8.[(-> af''16 ef''8 f'']) |
    \acciaccatura df''8 gf''4 gf'' |
    gf''8[-. af''-. bf''-. cf''']-. |
    bf''8-. bf''-. r gf'' |
    bf''8.[(-> af''16 ef''8 df'']) |
    \acciaccatura df''8 gf''2 |
    << { gf'8[-. af'-. bf'-. cf'']-. } \\ { df'2 } >> |
    << { <df' gf' bf'>4\( <df' gf'> | af'4 bf'\) } \\
       { s2 | <cf' gf'>4 <df' f'> } >> |
    << { s2 | gf'8[-. af'-. bf'-. cf'']-. } \\
       { \stemUp <bf df' gf'>8^\( ef' df'4\)->~ | \stemDown df'2 } >> |
    % http://lists.gnu.org/archive/html/lilypond-user/2018-02/msg00302.html
    \voiceOne <df' gf' bf'>4\( <df' gf'> |
    <c' gf' bf'>4 <<
                    { \voiceTwo gf'8 f'\) }
                    \new Voice { \voiceOne af'4 }
                    \new Voice { \voiceFour cf'4 }
                  >> |
    \oneVoice <bf gf'>2 |
    gf''8[-. af''-. bf''-. cf''']-. |
    bf''8-. bf''-. r gf'' |
    bf''8.[(-> af''16 ef''8 f'']) |
    \acciaccatura df''8 gf''4 gf'' |
    gf''8[-. af''-. bf''-. cf''']-. |
    bf''8-. bf''-. r gf'' |
    bf''8.[(-> af''16 ef''8 df'']) |
    \acciaccatura df''8 gf''2
    \bar "||"
    <ef'' ef'''>4. \tuplet 3/2 { df'''16\(( ef''' df''') } |
    <bf' bf''>2\) |
    <af' af''>4. \tuplet 3/2 { gf''16\(( af'' gf'') } |
    <ef' ef''>2\) |
    <ef' ef''>8[-. <f' f''>-. <gf' gf''>-. <af' af''>]-. |
    <gf' gf''>8-. r <ef' ef''>-. r |
    <ef' ef''>2-^~ |
    <ef' ef''>2 |
    \tempo "Meno mosso."
    ef''4.^\conDolore \tuplet 3/2 { df''16( ef'' df'')} |
    bf'2 |
    af'4.\( \tuplet 3/2 { gf'16( af' gf') } |
    ef'2\)\fermata |
    << { ef'8[\(^\espressivo f' gf' af'] | gf'4 ef'\) } \\ { bf2( | cf'2) } >>
    <bf ef'>2~ |
    <bf ef'>2 |
    << { ef'8[\( f' gf' af'] | gf'4 ef'\) } \\ { cf'2~ | cf'2 } >>
    <bf ef'>2~ |
    <bf ef'>2 |
    <cf' ef'>8\([-. f'-. gf'-. g']-. |
    af'8[-. bf'-. cf''-. df'']-. |
    ef''8[-. f''-. gf''!-. g'']-. |
    af''8[-. bf''-. cf'''-. df'''\)]-. |
    \tempo "Tempo I."
    \repeat tremolo 8 { <ef'' ef'''>32 f''' } |
    \repeat tremolo 8 { ef'''32 f''' } |
    \ottava #1 \set Staff.ottavation = #"8"
    \repeat tremolo 8 { <ef''' ef''''>32 f'''' } |
    \repeat tremolo 8 { ef''''32 f'''' } |
    bf'''8 \ottava #0 r r4 |
    \ottava #1 \set Staff.ottavation = #"8"
    <af'' d''' bf'''>8 \ottava #0 r r4 |
    <ef'' gf'' bf'' ef'''>2\fermata
    \bar "|."
  }
  \new Dynamics {
    s2\ff
    s2*7
    s2\mf
    s2*2
    s2\fz
    s4\fz s4\fz
    s4\fz s4\fz
    s4\fz s4\fz
    s2\fz
    s2\ff
    s2*7
    \bar "||"
    s2\mp
    s2*3
    s4 s4\dim
    s2*3
    s2\pp
    s2*5
    s4 s8\fz s8\>
    s8 s4.\!
    s2\mp
    s2*3
    s4 s4\dim
    s2*3
    s2\pp
    s2*5
    s4 s8\fz s8\>
    s8 s4.\!
    \bar "||"
    s2\ff
    s2*7
    \bar "||"
    s2\mf
    s2*2
    s4 s4\fz
    s2
    s2\dim
    s2*2
    s2\p
    s2*7
    s2\mf
    s2*2
    s4 s4\fz
    s2
    s2\dim
    s2*2
    s2\p
    s2*7
    \bar "||"
    s2\ff
    s2*7
    s2\mp
    s4. s8\>
    s8. s16\! s4
    s2*9
    s2\p
    s4. s8\crescMoltoEdAccel
    s2*2
    s2\ff
    s4 s4\>
    s4\! s4\<
    s8 s8\! s4
    s2\ff
    s2\f
    s2\ff
    \bar "|."
  }
  \new Staff = "left" {
    \clef bass
    \key ef \minor
    \time 2/4
    << { ef,8.[_( bf,16 gf8. bf,16]) } \\ { ef,2_\conPed } >> |
    << { ef,8.[_( bf,16 gf8. bf,16]) } \\ { ef,2\sustainOn } >> |
    << { ef,8.[_( cf16 gf8. cf16]) } \\ { ef,2\sustainOn } >> |
    << { ef,8.[_( cf16 gf8. cf16]) } \\ { ef,2\sustainOn } >> |
    << { ef,8.[_( bf,16 gf8. bf,16]) } \\ { ef,2\sustainOn } >> |
    << { ef,8.[_( bf,16 gf8. bf,16]) } \\ { ef,2 } >> |
    << { ef8[-. f-. gf-. af-.] |
    gf8-. f-. ef4 } \\ { ef,2~ | ef,2 } >> |
    \clef treble
    << { gf'8[-. f'-. ef'-. eff'-.] } \\ { cf'2 } >> |
    <bf df'>4 <af ef'> |
    <gf ef'>4 << { ef'4 } \\ { af8 bf } >> |
    <cf' ef'>2 |
    << { gf'8 ef' gf' ef' } \\ { cf'8 r bf r } >> |
    << { gf'8 ef' gf' ef' } \\ { af8 r gf r } >> |
    << { ef'8[ gf' f' gf'] } \\ { af8 r a r } >> |
    <bf d' af'>2 |
    \clef bass
    << { ef,8.[_( bf,16 gf8. bf,16]) } \\ { ef,2\sustainOn } >> |
    << { ef,8.[_( bf,16 gf8. bf,16]) } \\ { ef,2\sustainOn } >> |
    << { ef,8.[_( cf16 gf8. cf16]) } \\ { ef,2\sustainOn } >> |
    << { ef,8.[_( cf16 gf8. cf16]) } \\ { ef,2\sustainOn } >> |
    << { ef,8.[_( bf,16 gf8. bf,16]) } \\ { ef,2\sustainOn } >> |
    << { ef,8.[_( bf,16 gf8. bf,16]) } \\ { ef,2 } >> |
    << { ef8[-. f-. gf-. af-.] |
    gf8-. f-. ef4 } \\ { ef,2~ | ef,2 } >>
    \bar "||"
    <ef g>2 |
    << { r4 g } \\ { c2 } >> |
    f,2 |
    bf,4( ef,) |
    <ef g>2 |
    << { s4 g } \\ { <c bf>2 } >> |
    f,8[ f cf! bf,] |
    \tuplet 3/2 { ef,8( ef bf, } ef,8) r |
    c8 r <c' ef'> r |
    ef8 r <bf g'> r |
    af,8 r <ef af c'> r |
    ef,8 r <g ef'> r |
    c8 r <c' ef'> r |
    ef8 r <bf g'> r |
    bf,8 r <af d'>\sustainOn r |
    ef,8\sustainOn r <g ef'> r |
    <ef g>2 |
    << { r4 g } \\ { c2 } >> |
    f,2 |
    bf,4( ef,) |
    <ef g>2 |
    << { s4 g } \\ { <c bf>2 } >> |
    f,8[ f cf! bf,] |
    \tuplet 3/2 { ef,8( ef bf, } ef,8) r |
    c8 r <c' ef'> r |
    ef8 r <bf g'> r |
    af,8 r <ef af c'> r |
    ef,8 r <g ef'> r |
    c8 r <c' ef'> r |
    ef8 r <bf g'> r |
    bf,8 r <af d'>\sustainOn r |
    ef,8\sustainOn r <g ef'> r
    \bar "||"
    << { ef,8.[_( bf,16 gf!8. bf,16]) } \\ { ef,2_\conPed } >> |
    << { ef,8.[_( bf,16 gf8. bf,16]) } \\ { ef,2 } >> |
    << { ef,8.[_( cf16 gf8. cf16]) } \\ { ef,2 } >> |
    << { ef,8.[_( cf16 gf8. cf16]) } \\ { ef,2 } >> |
    << { ef,8.[_( bf,16 gf8. bf,16]) } \\ { ef,2 } >> |
    << { ef,8.[_( bf,16 gf8. bf,16]) } \\ { ef,2 } >> |
    << { ef8[-. f-. gf-. af-.] |
    gf8-. f-. ef4 } \\ { ef,2~ | ef,2 } >>
    \bar "||"
    <gf bf>2 |
    << { r4 bf } \\ { ef2 } >> |
    af,4 df |
    gf,4 bf~ |
    <gf bf>2 |
    << { r4 bf } \\ { ef2 } >> |
    af,8 af df4 |
    gf,8[ df gf,] r |
    ef8 r \clef treble <ef' gf'> r |
    gf8 r <df' bf'> r \clef bass |
    cf8 r \clef treble <ef' gf'> r \clef bass |
    gf8 r \clef treble <df' bf'> r \clef bass |
    ef8 r <ef' gf'> r |
    gf8 r \clef treble <df' bf'> r \clef bass |
    df8 r <cf' f'> r |
    gf,8 r <bf gf'> r |
    <gf bf>2 |
    << { r4 bf } \\ { ef2 } >> |
    af,4 df |
    gf,4 bf~ |
    <gf bf>2 |
    << { r4 bf } \\ { ef2 } >> |
    af,8 af df4 |
    gf,8[ df gf,] r |
    ef8 r \clef treble <ef' gf'> r |
    gf8 r <df' bf'> r \clef bass |
    cf8 r \clef treble <ef' gf'> r \clef bass |
    gf8 r \clef treble <df' bf'> r \clef bass |
    ef8 r <ef' gf'> r |
    gf8 r \clef treble <df' bf'> r \clef bass |
    df8 r <cf' f'> r |
    gf,8 r <bf gf'> r
    \bar "||"
    << { ef,8.[_( bf,16 gf8. bf,16]) } \\ { ef,2 } >> |
    << { ef,8.[_( bf,16 gf8. bf,16]) } \\ { ef,2 } >> |
    << { ef,8.[_( cf16 gf8. cf16]) } \\ { ef,2 } >> |
    << { ef,8.[_( cf16 gf8. cf16]) } \\ { ef,2 } >> |
    << { ef,8._( bf,16 gf8. bf,16) } \\ { ef,2 } >> |
    << { ef,8._( bf,16 gf8. bf,16) } \\ { ef,2 } >> |
    << { ef8[-. f-. gf-. af-.] |
    gf8-. f-. ef4 } \\ { ef,2~ | ef,2 } >> |
    ef,8.[( bf,16 gf8]) r |
    gf,8.[( df16 gf8]) r |
    af,8.[( ef16 cf'8]) r |
    cf8.[( gf16 gf'8])\fermata r |
    <ef gf>2 |
    <af, ef>2 |
    << { ef2~ | ef2 } \\
       { gf,8[-._\marcato af,-. bf,-. cf]-. | bf,8-. af,-. gf,4 } >>
    <af, ef>2~ |
    <af, ef>2 |
    << { ef2~ | ef2 } \\ { gf,8[-. af,-. bf,-. cf]-. | bf,8-. af,-. gf,4 } >>
    <af, gf>8 r <af, ef cf'>4\arpeggio~\sustainOn |
    <af, ef cf'>2~ |
    <af, ef cf'>2~ |
    <af, ef cf'>2\sustainOff |
    ef,8.[\sustainOn bf,16 ef8-. f]-. |
    gf8[-. bf-. ef'-. f']-. \clef treble |
    gf'8[-. bf'-. ef''-. f'']-. |
    gf''8[-. bf''-. ef'''-. f''']-. |
    gf'''8 r r4 |
    <bf ef' gf'>8 r r4\sustainOff \clef bass |
    ef,16\([\sustainOn bf, ef gf bf
        \change Staff = "right" ef' gf'8]\)\fermata
    % TODO: sustainOff
    \bar "|."
  }
>>
