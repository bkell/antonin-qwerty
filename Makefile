antonin-qwerty.pdf: antonin-qwerty.tex keyboard-dvorak.mps keyboard-qwerty.mps \
    KB_DSKtypewriter.pdf KB_United_States.pdf sheet_music/humoresque_no_7.pdf \
    sheet_music/humoresque_no_7_qwertified_manual_tweaks.pdf
	pdflatex antonin-qwerty.tex
	pdflatex antonin-qwerty.tex

keyboards.pdf: keyboards.tex keyboard-dvorak.mps keyboard-qwerty.mps
	pdftex keyboards.tex

keyboard-dvorak.mps: keyboards.mp
	mpost keyboards.mp

keyboard-qwerty.mps: keyboards.mp
	mpost keyboards.mp
